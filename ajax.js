$('#feedback_form').on('submit',function(event){
    event.preventDefault();
    var file_data = $('#files').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('action', 'set_task');
    form_data.append('name', $('#name').val());
    form_data.append('email', $('#inputEmail').val());
    form_data.append('telpick', $('#telephone').val());
    form_data.append('tel', $('#tel').val());
    form_data.append('task', $('#task').val());
    form_data.append('serialized',$('#feedback_form').serialize());

    $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: form_data,
        processData: false,
        contentType: false,
        success : function(data){
            if(data == ''){
                document.location.href = '/wp-content/themes/outsourcing-team/send';
            }
        }
    });
});